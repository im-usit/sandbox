package com.ingrammicro.gpp.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

public class BaseClass {

 	public static WebDriver driver;
 	
	@BeforeClass
	public void TestBaseClass() {

		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "\\ChromeDriver\\chromedriver.exe");
//		 driver = new HtmlUnitDriver();
		 	
//		ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--no-sandbox");
////        chromeOptions.addArguments("--headless");
//        chromeOptions.setHeadless(true);
        
        
//		 driver = new ChromeDriver(chromeOptions); 
		
		driver = new ChromeDriver();
		
		driver.get("http://pricingmgmt.gpp.corporate.ingrammicro.com:8680/static/");
		driver.manage().window().maximize();
		Reporter.log("GPP Done Succesfully ");
		
		System.out.println("Title : "+driver.getTitle());

	}
	
	
	@AfterTest
	public void TestAfterTest() throws InterruptedException {
//		Thread.sleep(5000);
//		driver.close();
	}

	}

}

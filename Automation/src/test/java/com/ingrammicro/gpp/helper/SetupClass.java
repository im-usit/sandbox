package com.ingrammicro.gpp.helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

public class SetupClass {

 	public static WebDriver driver;
 	public static HSSFWorkbook wb ;
 	public static HSSFSheet  sheet;
 	
	@BeforeClass
	public void TetsSetupClass() {

		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "\\ChromeDriver\\chromedriver.exe");
//		 driver = new HtmlUnitDriver();
		 	
//		ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--headless");
//        chromeOptions.addArguments("window-size=1920,1080");
//		 driver = new ChromeDriver(chromeOptions); 
		
		driver = new ChromeDriver();
		
//		driver.get("http://pricingmgmt.gpp.corporate.ingrammicro.com:8680/static/");
		driver.get("http://localhost:4200/static");
		
		driver.manage().window().maximize();
		Reporter.log("GPP URL open Succesfully ",true);
		
		System.out.println("Title : "+driver.getTitle());
		

	}
	
	public String getCell(int rowNum, int ColNum) throws IOException {
		FileInputStream file = new FileInputStream("D:\\Automation\\excel\\testdata.xls");
		wb = new HSSFWorkbook(file);
		sheet = wb.getSheet("Sheet1");
		String Celldata = sheet.getRow(rowNum).getCell(ColNum).getStringCellValue();
		return Celldata;
	}
	
	public Integer getIntegerCellData(int rowNum, int ColNum) throws IOException {
		FileInputStream file = new FileInputStream("D:\\Automation\\excel\\testdata.xls");
		wb = new HSSFWorkbook(file);
		sheet = wb.getSheet("Sheet1");
		Integer Celldata = (int) sheet.getRow(rowNum).getCell(ColNum).getNumericCellValue();
		return Celldata;
	}
	

	
	
	@AfterTest
	public void TestAfterTest() throws InterruptedException {
//		Thread.sleep(5000);
//		driver.close();
	}

}

package com.ingrammicro.gpp.POM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonElements {


	public static WebElement element=null;

	//Segment Menu Options
	public static WebElement SegmentMenuDropdown(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[text()='Segment ']//*[text()='arrow_drop_down']"));
		return element;
	}
	
	//Search Segment
	public static WebElement Search_Segment(WebDriver driver) {
		element =	driver.findElement(By.xpath("//button[text()='Search Segment']"));
		return element;
	}
	
	//Vendor Override
	public static WebElement Vendor_Override(WebDriver driver) {
		element =	driver.findElement(By.xpath("//button[text()='Vendor Override']"));
		return element;
	}
	
	public static WebElement Search_Segment_field(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@data-placeholder='Enter Segment Name ']"));
		return element;
	}
	
	//edit
	public static WebElement edit_record(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[text()='edit']"));
		return element;
	}
	
	//add_box
	public static WebElement add_rule(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[text()=' add_box ']"));
		return element;
	}
	
	//Business Justification Comment button
	public static WebElement Comment_Button(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@mattooltip='Edit Comment']"));
		return element;
	}
	
	//costCode
	public static WebElement CostCode(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@formcontrolname='costCode']"));
		return element;
	}
	
	//modifierPercent 
	public static WebElement ModifierPercent(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@formcontrolname='modifierPercent']"));
		return element;
	}
	
	//minimumAmount
	public static WebElement MinimumAmount(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@formcontrolname='minimumAmount']"));
		return element;
	}
	
	//activation date/time Button
	public static WebElement ActivationDate(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@formcontrolname='activation']"));
		return element;
	}
	
	//expiration date/time Button
	public static WebElement ExpirationDate(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@formcontrolname='expiration']"));
		return element;
	}
	
	//Save rule entry
	public static WebElement Save_rule_entry(WebDriver driver) {
//		element =	driver.findElement(By.xpath("//*[text()='check_circle']"));
		element =	driver.findElement(By.xpath("//*[@mattooltip='Save Changes']"));
		
		return element;
	}	
	
	//Rule Validation Message 
	public static List<WebElement> RuleValidationMessage(WebDriver driver) {
		List<WebElement> element = driver.findElements(By.xpath("//*[@class='mat-error ng-star-inserted']"));
		return element;
	}	
	
	//Cancel rule entry
	public static WebElement Cancel_rule_entry(WebDriver driver) {
		element =	driver.findElement(By.xpath("//*[@mattooltip='Cancel Changes']"));
		return element;
	}
	
	//Save Button
	public static WebElement Save(WebDriver driver) {
//		element =	driver.findElement(By.xpath("//span[text()=' Save ']"));
		element =	driver.findElement(By.xpath("//span[text()=' Save ']//following::span[@class='mat-ripple mat-button-ripple']"));
		return element;
	}
	
	//Cancel Button
	public static WebElement Cancel(WebDriver driver) {
//		element =	driver.findElement(By.xpath("//span[text()='Cancel']"));
		element =	driver.findElement(By.xpath("//span[text()='Cancel']//following::span[@class='mat-ripple mat-button-ripple']"));
		return element;
	}
	
	
	
}

package com.ingrammicro.gpp.segment;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.ingrammicro.gpp.POM.CommonElements;
import com.ingrammicro.gpp.helper.SetupClass;

public class Segment_VendorOverride extends SetupClass {

	@Test
	public void TetsSegment_VendorOverride() throws InterruptedException, IOException {

		Reporter.log("Segment_VendorOverride Script started ", true);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		Date Tran_date = new Date();
		DateFormat df = new SimpleDateFormat("dd/MM/yy");
		String systemdate= df.format(Tran_date);

		String Seg_number = getCell(2, 0);
		System.out.println("Seg_number : " + Seg_number);

		CommonElements.SegmentMenuDropdown(driver).click();

		Thread.sleep(1000);
		WebElement SearchSegment = CommonElements.Search_Segment(driver);
		String Segment = SearchSegment.getText();
		Reporter.log("Segment Option : " + Segment, true);

		Assert.assertEquals(Segment, "Search Segment");

		Thread.sleep(1000);
		SearchSegment.click();

		Thread.sleep(1000);
		CommonElements.Search_Segment_field(driver).click();
		CommonElements.Search_Segment_field(driver).clear();
		Thread.sleep(1000);
		CommonElements.Search_Segment_field(driver).sendKeys(Seg_number);

		//		//List of record display in search option
		//		Thread.sleep(500);
		//		List<WebElement> list= driver.findElements(By.xpath("//*[@role='option']"));
		//		for(WebElement e : list) {
		//			System.out.println(e.getText());
		//		Reporter.log(e.getText() ,true);
		//		}

		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@ng-reflect-value='" + Seg_number + "']")).click();

		// Validates whether the Vendor_Override option is Disabled
		Thread.sleep(500);
		CommonElements.SegmentMenuDropdown(driver).click();

		Thread.sleep(1000);
		if (CommonElements.Vendor_Override(driver).isEnabled()) {
			Reporter.log("Vendor_Override is enable ", true);
		} else {
			Reporter.log("Vendor_Override is desable ", true);
			Thread.sleep(500);
			executor.executeScript("arguments[0].click();", CommonElements.SegmentMenuDropdown(driver));
		}

		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", CommonElements.edit_record(driver));
		//		CommonElements.edit_record(driver).click();
		Reporter.log("Segment selected for edit", true);

		Thread.sleep(500);
		CommonElements.SegmentMenuDropdown(driver).click();

		// Validates whether the Vendor_Override option is Disabled
		Thread.sleep(1000);
		if (CommonElements.Vendor_Override(driver).isEnabled()) {
			Reporter.log("Vendor_Override is enable ", true);
		} else {
			Reporter.log("Vendor_Override is desable ", true);
			Thread.sleep(500);
			executor.executeScript("arguments[0].click();", CommonElements.SegmentMenuDropdown(driver));
		}

		Thread.sleep(500);
		WebElement Vendor_Override = CommonElements.Vendor_Override(driver);
		String VendorOverride = Vendor_Override.getText();
		Reporter.log("Selected Rule : " + VendorOverride, true);
		Assert.assertEquals(VendorOverride, "Vendor Override");

		Thread.sleep(1000);
		Vendor_Override.click();

		Thread.sleep(1000);
		// Validates whether the button is Enabled
		//		Assert.assertEquals(true, CommonElements.add_rule(driver).isEnabled()); 
		if (CommonElements.add_rule(driver).isEnabled()) {
			Reporter.log("add rule Button is enable ", true);
			String AddRule_ReflectMessage = CommonElements.add_rule(driver).getAttribute("ng-reflect-message");
			Reporter.log("add rule Button Reflect Message : " + AddRule_ReflectMessage, true);
		} else {
			Reporter.log("add rule Button is desable ", true);
			String AddRule_ReflectMessage = CommonElements.add_rule(driver).getAttribute("ng-reflect-message");
			Reporter.log("add rule Button Reflect Message : " + AddRule_ReflectMessage, true);
		}

		// Check whether rule is added/not
		try {
			// Segment/Vendor Override panel click
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[text()='Segment/Vendor Override ']")).click();
			// copy message if no rule is added
			Thread.sleep(1000);
			Reporter.log(driver.findElement(By.xpath("//*[@class='mat-cell']")).getText(), true);

		} catch (Exception e) {
			// check number of rule added
			Thread.sleep(1000);
			List<WebElement> Rule_Count = driver.findElements(By.xpath("//tbody[@role='rowgroup']/tr"));
			int count = Rule_Count.size();
			Reporter.log("Total numer ro vender rule present in segment are : " + count, true);

		}

		// Print all fields
		Reporter.log("Fields precent in Vendor Override Rules ", true);
		Thread.sleep(1000);
		List<WebElement> list1 = driver.findElements(By.xpath("//*[@role='rowgroup']/tr/th"));
		for (WebElement e : list1) {
			Reporter.log(e.getText(), true);
		}
		/*
		 * Thread.sleep(1000); int total_elements = list1.size();
		 * System.out.println("Total elements Count is = " + total_elements);
		 * 
		 * List<String> expected = Arrays.asList("Id", "Vendor", "Date", "Price Rule",
		 * "Eligibility Flags", "Action", " ", "Number", "Name", "Velocity Code",
		 * "Activation Time", "Expiration Time", "Cost Code", "%", "Min Amt", "Pricing",
		 * "Qty Brk", "Floor", "ACOP", " "); Thread.sleep(1000); for(int i=0;
		 * i<list1.size(); i++) { if(list1.get(i).getText().equals(expected.get(i)))
		 * Reporter.log("Matched " ,true); else Reporter.log("Didn't match " ,true); }
		 */
		//		Thread.sleep(1000);
		//        String[]  expe = {"Id", "Vendor", "Date", "Price Rule", "Eligibility Flags", "Action", " ", "Number", "Name", "Velocity Code", "Activation Time", "Expiration Time", "Cost Code", "%", "Min Amt", "Pricing", "Qty Brk", "Floor", "ACOP", " "};	
		//        int count = 0;
		//        for (WebElement ele: list1) {
		//            count++;
		//            Assert.assertTrue(ele.getText().equals(expe[count])); 
		//            Reporter.log("Matched " ,true);
		//        }

		Thread.sleep(1000);
		CommonElements.add_rule(driver).click();

		// Validates whether the button is Disabled
		Thread.sleep(1000);
		//		Assert.assertEquals(true, CommonElements.add_rule(driver).isEnabled()); 
		if (CommonElements.add_rule(driver).isEnabled()) {
			Reporter.log("add rule Button is enable ", true);
			String AddRule_ReflectMessage = CommonElements.add_rule(driver).getAttribute("ng-reflect-message");
			Reporter.log("add rule Button Reflect Message : " + AddRule_ReflectMessage, true);
		} else {
			Reporter.log("add rule Button is desable ", true);
			String AddRule_ReflectMessage = CommonElements.add_rule(driver).getAttribute("ng-reflect-message");
			Reporter.log("add rule Button Reflect Message : " + AddRule_ReflectMessage, true);
		}

		Thread.sleep(500);
		if (CommonElements.Comment_Button(driver).isEnabled()) {
			Reporter.log("Comment_Button is enable ", true);
		} else {
			Reporter.log("Comment_Button is desable ", true);
		}

		Thread.sleep(500);
		if (CommonElements.Save_rule_entry(driver).isEnabled()) {
			Reporter.log("Save_rule_entry Button is enable ", true);
		} else {
			Reporter.log("Save_rule_entry Button is desable ", true);
		}

		Thread.sleep(500);
		if (CommonElements.Cancel_rule_entry(driver).isEnabled()) {
			Reporter.log("Cancel rule Button Button is enable ", true);
			String CancelRule_ReflectMessage = CommonElements.Cancel_rule_entry(driver)
					.getAttribute("ng-reflect-message");
			Reporter.log("Cancel rule Button Reflect Message : " + CancelRule_ReflectMessage, true);
		} else {
			Reporter.log("Cancel rule Button Button is enable ", true);
			String CancelRule_ReflectMessage = CommonElements.Cancel_rule_entry(driver)
					.getAttribute("ng-reflect-message");
			Reporter.log("Cancel rule Button Reflect Message : " + CancelRule_ReflectMessage, true);
		}

		Thread.sleep(500);
		if (CommonElements.Save(driver).isEnabled()) {
			Reporter.log("Save Segment Button is enable ", true);
		} else {
			Reporter.log("Save Segment Button is desable ", true);
		}

		Thread.sleep(500);
		if (CommonElements.Cancel(driver).isEnabled()) {
			Reporter.log("Cancel Segment Button is enable ", true);
		} else {
			Reporter.log("Cancel Segment Button is desable ", true);
		}

		// check validation when vendor id in blank
		try {
			// VendorID
			Thread.sleep(500);
			WebElement VendorID = driver.findElement(By.xpath("//*[@id='vendorNumber0']"));
			VendorID.click();
			VendorID.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement ValidationMessage = driver.findElement(By.xpath("//*[@class='mat-error ng-star-inserted']"));
			String VendorValidationMessage = ValidationMessage.getText();
			Reporter.log("Vendor number Validation Message : " + VendorValidationMessage, true);
			Assert.assertEquals(VendorValidationMessage, "Vendor number is required.");

			// CostCode
			Thread.sleep(500);
			WebElement CostCode = CommonElements.CostCode(driver);
			CostCode.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement CostCodeValidationMessage = CommonElements.RuleValidationMessage(driver).get(2);
			String CostCode_ValidationMessage = CostCodeValidationMessage.getText();
			Reporter.log("Cost Code Validation Message : " + CostCode_ValidationMessage, true);
			Assert.assertEquals(CostCode_ValidationMessage, "Cost Code is required.");

			// modifierPercent
			Thread.sleep(500);
			WebElement modifierPercent = CommonElements.ModifierPercent(driver);
			modifierPercent.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement modifierPercentValidationMessage = CommonElements.RuleValidationMessage(driver).get(4);
			String modifierPercent_ValidationMessage = modifierPercentValidationMessage.getText();
			Reporter.log("Modifier Percent Validation Message : " + modifierPercent_ValidationMessage, true);
			Assert.assertEquals(modifierPercent_ValidationMessage, "Modifier Percent is required.");

			// modifierPercent
			Thread.sleep(500);
			WebElement minimumAmount = CommonElements.MinimumAmount(driver);
			minimumAmount.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement minimumAmountValidationMessage = CommonElements.RuleValidationMessage(driver).get(6);
			String minimumAmount_ValidationMessage = minimumAmountValidationMessage.getText();
			Reporter.log("Minimum Amount is required. : " + minimumAmount_ValidationMessage, true);
			Assert.assertEquals(minimumAmount_ValidationMessage, "Minimum Amount is required.");

			Thread.sleep(500);
			if (CommonElements.Save_rule_entry(driver).isEnabled()) {
				Reporter.log("Save_rule_entry Button is enable ", true);
			} else {
				Reporter.log("Save_rule_entry Button is desable ", true);
			}

			Thread.sleep(500);
			if (CommonElements.Comment_Button(driver).isEnabled()) {
				Reporter.log("Comment_Button is enable ", true);
			} else {
				Reporter.log("Comment_Button is desable ", true);
			}

		} catch (Exception e) {
			Reporter.log("Please check validation when vendor id is blank in  rule", true);
		}

		// check validation when enter invalid vendor id
		try {
			String Vendor_ID = getCell(1, 1);
			System.out.println("Vendor ID : " + Vendor_ID);

			Thread.sleep(1000);
			WebElement VendorID = driver.findElement(By.xpath("//*[@id='vendorNumber0']"));
			VendorID.click();
			VendorID.sendKeys(Vendor_ID);
			VendorID.sendKeys(Keys.TAB);

			Thread.sleep(1000);
			WebElement ValidationMessage = driver.findElement(By.xpath("//*[@class='mat-error ng-star-inserted']"));
			String VendorValidationMessage = ValidationMessage.getText();
			Reporter.log("Vendor number is required : " + VendorValidationMessage, true);

			Thread.sleep(1000);
			if (CommonElements.Save_rule_entry(driver).isEnabled()) {
				Reporter.log("Save_rule_entry Button is enable ", true);
			} else {
				Reporter.log("Save_rule_entry Button is desable ", true);
			}

			Thread.sleep(1000);
			if (CommonElements.Comment_Button(driver).isEnabled()) {
				Reporter.log("Comment_Button is enable ", true);
			} else {
				Reporter.log("Comment_Button is desable ", true);
			}

		} catch (Exception e) {
			Reporter.log("Please check validation when enter invalid vendor id in rule", true);
		}

		//Issue present need to report this issue
		// check cancel rule option
		try {
			String Vendor_ID = getCell(2, 1);
			System.out.println("Vendor ID : " + Vendor_ID);

			int modifier_percent = (int) getIntegerCellData(1, 2);
			String Modifier_Percent = String.valueOf(modifier_percent);
			System.out.println("Modifier_Percent : " + Modifier_Percent);
			int minimum_amount = (int) getIntegerCellData(1, 3);
			String Minimum_Amount = String.valueOf(minimum_amount);
			System.out.println("Minimum_Amount : " + Minimum_Amount);

			Thread.sleep(1000);
			WebElement VendorID = driver.findElement(By.xpath("//*[@id='vendorNumber0']"));
			VendorID.click();
			VendorID.clear();
			VendorID.sendKeys(Vendor_ID);
			VendorID.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement CostCode = CommonElements.CostCode(driver);
			CostCode.click();

			Thread.sleep(500);
			driver.findElement(By.xpath("//*[text()=' [4]-Landed Cost ']")).click();

			// modifierPercent
			Thread.sleep(500);
			WebElement modifierPercent = CommonElements.ModifierPercent(driver);
			modifierPercent.sendKeys(Modifier_Percent);
			modifierPercent.sendKeys(Keys.TAB);

			// minimumAmount
			Thread.sleep(500);
			WebElement minimumAmount = CommonElements.MinimumAmount(driver);
			minimumAmount.sendKeys(Minimum_Amount);
			minimumAmount.sendKeys(Keys.TAB);

			Thread.sleep(500);
			CommonElements.Cancel_rule_entry(driver).click();

			// Check whether rule is added/not
			try {
				// Segment/Vendor Override panel click
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[text()='Segment/Vendor Override ']")).click();
				// copy message if no rule is added
				Thread.sleep(1000);
				Reporter.log(driver.findElement(By.xpath("//*[@class='mat-cell']")).getText(), true);

			} catch (Exception e) {
				// check number of rule added
				Thread.sleep(1000);
				List<WebElement> Rule_Count = driver.findElements(By.xpath("//tbody[@role='rowgroup']/tr"));
				int count1 = Rule_Count.size();
				Reporter.log("Total numer ro vender rule present in segment are : " + count1, true);

			}

		} catch (Exception e) {
			Reporter.log("Please check cancel rule option", true);
		}

		// Check validation when enter invalid percentage and Amount in rule
		try {
			String Vendor_ID = getCell(2, 1);
			System.out.println("Vendor ID : " + Vendor_ID);
			String Modifier_Percent = getCell(2, 2);
			System.out.println("Modifier_Percent : " + Modifier_Percent);
			String Minimum_Amount = getCell(2, 3);
			System.out.println("Minimum_Amount : " + Minimum_Amount);

			//			int modifier_percent =(int)getIntegerCellData(2, 2);
			//			String Modifier_Percent =	String.valueOf(modifier_percent);	
			//			System.out.println("Modifier_Percent : "+Modifier_Percent);
			//			int minimum_amount =(int)getIntegerCellData(2, 3);
			//			String Minimum_Amount=	String.valueOf(minimum_amount);
			//			System.out.println("Minimum_Amount : "+Minimum_Amount);

			Thread.sleep(1000);
			CommonElements.add_rule(driver).click();

			Thread.sleep(1000);
			WebElement VendorID = driver.findElement(By.xpath("//*[@id='vendorNumber0']"));
			VendorID.click();
			VendorID.clear();
			VendorID.sendKeys(Vendor_ID);
			VendorID.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement CostCode = CommonElements.CostCode(driver);
			CostCode.click();

			Thread.sleep(500);
			driver.findElement(By.xpath("//*[text()=' [4]-Landed Cost ']")).click();

			// modifierPercent
			Thread.sleep(500);
			WebElement modifierPercent = CommonElements.ModifierPercent(driver);
			modifierPercent.clear();
			modifierPercent.sendKeys(Modifier_Percent);
			modifierPercent.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement modifierPercentValidationMessage = CommonElements.RuleValidationMessage(driver).get(1);
			String modifierPercent_ValidationMessage = modifierPercentValidationMessage.getText();
			Reporter.log("Modifier Percent Validation Message : " + modifierPercent_ValidationMessage, true);
			Assert.assertEquals(modifierPercent_ValidationMessage, "Enter valid decimal value(-99.99 to 99.99)");

			// modifierPercent
			Thread.sleep(500);
			WebElement minimumAmount = CommonElements.MinimumAmount(driver);
			minimumAmount.clear();
			minimumAmount.sendKeys(Minimum_Amount);
			minimumAmount.sendKeys(Keys.TAB);

			Thread.sleep(500);
			WebElement minimumAmountValidationMessage = CommonElements.RuleValidationMessage(driver).get(3);
			String minimumAmount_ValidationMessage = minimumAmountValidationMessage.getText();
			Reporter.log("Minimum Amount Validation Message : " + minimumAmount_ValidationMessage, true);
			Assert.assertEquals(minimumAmount_ValidationMessage, "Enter valid decimal value(upto .99)");

		} catch (Exception e) {
			Reporter.log("Please check validation when enter invalid percentage and Amount in rule", true);
		}



		// Check validation when enter invalid percentage and Amount in rule 
//		try {
//
//			String Vendor_ID= getCell(2, 1);
//			System.out.println("Vendor ID : "+Vendor_ID);
//
//			double modifier_percent3 =(double)getIntegerCellData(3, 2); String
//			Modifier_Percent3 = String.valueOf(modifier_percent3);
//			System.out.println("Modifier_Percent : "+Modifier_Percent3);
//
//			double minimum_amount3 =(double)getIntegerCellData(3, 3); String
//			Minimum_Amount3= String.valueOf(minimum_amount3);
//			System.out.println("Minimum_Amount : "+Minimum_Amount3);
//
//			double modifier_percent4 =(double)getIntegerCellData(4, 2); String
//			Modifier_Percent4 = BigDecimal.valueOf(modifier_percent4).toPlainString();
//			System.out.println("Modifier_Percent : "+Modifier_Percent4);
//
//			Thread.sleep(1000); WebElement VendorID =
//			driver.findElement(By.xpath("//*[@id='vendorNumber0']")); VendorID.click();
//			VendorID.clear(); VendorID.sendKeys(Vendor_ID); VendorID.sendKeys(Keys.TAB);
//
//			Thread.sleep(500); WebElement CostCode = CommonElements.CostCode(driver);
//			CostCode.click();
//
//			Thread.sleep(500);
//			driver.findElement(By.xpath("//*[text()=' [4]-Landed Cost ']")).click();
//
//			//modifierPercent 
//			Thread.sleep(500);
//			WebElement modifierPercent =CommonElements.ModifierPercent(driver); 
//			modifierPercent.clear();
//			modifierPercent.sendKeys(Modifier_Percent3);
//			modifierPercent.sendKeys(Keys.TAB);
//
//			Thread.sleep(500); WebElement modifierPercentValidationMessage =CommonElements.RuleValidationMessage(driver).get(1); 
//			String modifierPercent_ValidationMessage =modifierPercentValidationMessage.getText();
//			Reporter.log("Modifier Percent Validation Message : " +modifierPercent_ValidationMessage, true);
//			Assert.assertEquals(modifierPercent_ValidationMessage,"Enter valid decimal value(-99.99 to 99.99)");
//
//			// modifierPercent 
//			Thread.sleep(500); 
//			WebElement minimumAmount =CommonElements.MinimumAmount(driver); minimumAmount.clear();
//			minimumAmount.sendKeys(Minimum_Amount3); minimumAmount.sendKeys(Keys.TAB);
//
//			Thread.sleep(500); WebElement minimumAmountValidationMessage =
//			CommonElements.RuleValidationMessage(driver).get(3);
//			String minimumAmount_ValidationMessage = minimumAmountValidationMessage.getText();
//			Reporter.log("Minimum Amount Validation Message : " +minimumAmount_ValidationMessage, true);
//			Assert.assertEquals(minimumAmount_ValidationMessage,"Enter valid decimal value(upto .99)");
//
//	       // modifierPercent 
//			Thread.sleep(500);
//			WebElement modifierPercent1 = CommonElements.ModifierPercent(driver); modifierPercent1.clear();
//				modifierPercent1.clear(); modifierPercent1.sendKeys(Modifier_Percent4);
//			modifierPercent1.sendKeys(Keys.TAB);
//
//			Thread.sleep(500); 
//			WebElement modifierPercentValidationMessage1 =CommonElements.RuleValidationMessage(driver).get(1); 
//			String modifierPercent_ValidationMessage1 =  modifierPercentValidationMessage1.getText();
//			Reporter.log("Modifier Percent Validation Message : " + modifierPercent_ValidationMessage1, true);
//			Assert.assertEquals(modifierPercent_ValidationMessage1, "Enter valid decimal value(-99.99 to 99.99)");
//
//		} catch (Exception e) { 
//			Reporter.log("Please check validation2 when enter invalid percentage and Amount in rule", true); 
//		}

		//check validation when select past date and time in Expiration in rule 
		try {

			// ActivationDate 
			Thread.sleep(500);
			WebElement ActivationDate =  CommonElements.ActivationDate(driver); 
			ActivationDate.clear();
			ActivationDate.sendKeys(systemdate); ActivationDate.sendKeys(Keys.TAB);

			// ExpirationDate 
			Thread.sleep(500);
			WebElement ExpirationDate = CommonElements.ExpirationDate(driver); 
			ExpirationDate.clear();
			ExpirationDate.sendKeys("02/31/2021, 11:30 PM");
			ExpirationDate.sendKeys(Keys.TAB);

			Thread.sleep(500); 
			WebElement CostCode = CommonElements.CostCode(driver);
			CostCode.click();

			Thread.sleep(500);
			driver.findElement(By.xpath("//*[text()=' [4]-Landed Cost ']")).click();

			Thread.sleep(500);
			WebElement DateValidationMessage = CommonElements.RuleValidationMessage(driver).get(0); String
			date_ValidationMessage = DateValidationMessage.getText();
			Reporter.log("Date Validation Message : " + date_ValidationMessage, true);
			Assert.assertEquals(date_ValidationMessage,"End Date must come after Start Date.");


		} catch (Exception e) { 
			Reporter.log("Please check validation when select past date and time in Expiration in rule", true);
		}


		Reporter.log("Segment_VendorOverride script completed succesfully ", true);
		//		driver.close();

	}
}

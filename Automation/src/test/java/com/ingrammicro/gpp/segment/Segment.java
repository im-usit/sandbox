package com.ingrammicro.gpp.segment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ingrammicro.gpp.POM.CommonElements;
import com.ingrammicro.gpp.helper.BaseClass;

import ExcelTest.ExcelReader;


public class Segment extends BaseClass{

	 @Test
	  public void TestSegment() throws InterruptedException, IOException {
			
			
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		FileInputStream file = new FileInputStream("D:\\Automation\\excel\\testdata.xls");
		
		HSSFWorkbook wb = new HSSFWorkbook(file);
		HSSFSheet sh = wb.getSheet("Sheet1");

		String Seg_name =sh.getRow(1).getCell(0).getStringCellValue();
		System.out.println("");
		System.out.println("Seg_name : "+Seg_name);
		String Seg_name2 =sh.getRow(2).getCell(0).getStringCellValue();
		System.out.println("");
		System.out.println("Seg_name2 : "+Seg_name2);
		
		CommonElements.SegmentMenuDropdown(driver).click();
		
		WebElement SearchSegment = CommonElements.Search_Segment(driver);
		String Segment = SearchSegment.getText();
		System.out.println("Segment Option : "+Segment);
		Assert.assertEquals(Segment, "Search Segment");
		
		Thread.sleep(1000);
		SearchSegment.click();

		Thread.sleep(1000);
		CommonElements.Search_Segment_field(driver).click();
		CommonElements.Search_Segment_field(driver).clear();
		Thread.sleep(1000);
		CommonElements.Search_Segment_field(driver).sendKeys(Seg_name);	
		
		
		
	}

}

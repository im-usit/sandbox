### Contribution guidelines ###

* This sandbox is for everyone to practice working with Bitbucket/Git
* Do not store any sensitive or proprietary content/information in this repository
* Clone to your machine
* Practice fetch/pull from / commit/push to upstream on the master or a branch to get the feeling
* Learn about merge, rebase, reset (advanced!)
* Learn about branches, commits, pull request, merges
* You can create a branch in Bitbucket, check it out it locally, modify something, commit, then create a pull request
* You can create a branch locally, push it to remote, make changes, commit, then create pull request 
* After necessary reviews, merge your branch changes to the master or other branch
* Create releases
* Use with your favored Git client
* Try Atlassian's Sourcetree utility or
* Try Eclipse's eGit extension/plugin
* IMPORTANT: Please edit this README.md file only if you want to add usefull information related to using Bitbucket and Git clients 

### Use with your Git clients ###

AUTHENTICATION

The online authentication to this repository is using single sign on (SSO), which is administered
by Ingram Micro. Git client applications that cannot use SSO must use a user/password combination which is
not available with SSO. Again, do not try use your corporate password with Git clients to
work with this repository.

CREATE APP PASSWORD IN BITBUCKET

Click on your Bitbucket user icon (bottom/left in the browser) and choose "Personal Settings".
In the middle column under "Access Management" click on "App Passwords". On the 
"App Passwords" page click on the "Create app password" button, enter a Label to help
you remember what the password is for, then under "Permissions" choose the desired access
(read/write...) level for the Git client you intend to use with the password. Once ready,
click on "Create". Make sure you copy the displayed password and store it somewhere safe,
this is the only time the password is displayed readable.

USE APP PASSWORD AND USERNAME

You can now use the user name and the created password combination with a Git client that
requires a user name and password.

```bash
git clone https://username:password@bitbucket.org/im-usit/keystore.git

git clone https://username@bitbucket.org/im-usit/keystore.git (for password prompt)
```

In Eclipse/EGit you can use the user and password fields when you create a repository in the
"Git Repositories" view.



### Who do I talk to? ###

* Repo owner (Gurmat Bhatia)
* Admins (e.g. Alex Savulov, James Luei)